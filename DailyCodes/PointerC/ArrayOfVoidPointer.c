#include<stdio.h>


void main(){

	int x = 10;
	char ch = 'A';
	double d = 30.85;

	
	void* arr[3] = {&x,&ch,&d};
	/*Representation
		------   ------  ------
	       x| 10 | ch|'A'| d|30.85|
		------   ------  ------
		100 103  200  201 300  307
	     
		--------------------
	    arr	| 100 |  200 | 300 |
		--------------------
		400    408    416   423

	*/
	printf("%ld\n",sizeof(arr));//8*3 = 24

	printf("%p\n",arr[0]);//100
	printf("%p\n",arr[1]);//200
	printf("%p\n",arr[2]);//300

	printf("%d\n",*((int*)arr[0]));//10
	printf("%c\n",*((char*)arr[1]));//'A'
	printf("%lf\n",*((double*)arr[2]));//30.85
				
}
