#include<stdio.h>


void main(){

	int arr1[3] = {1,2,3};
	int arr2[3] ={4,5,6};

	int (*ptr1)[3] = &arr1;
	int (*ptr2)[3] = &arr2;

	int (*ptr3[2])[3] = {&arr1,&arr2};
	for(int i=0;i<3;i++){

		printf("%d ",*(*ptr1 + i));
	}

/*

	int a = 10;
	int b = 20;
	int c = 30;

	int *ptr1 = &a;
	int *ptr2 = &b;
	int *ptr3 = &c;

	int* arr[3] = {ptr1,ptr2,ptr3};
	int* arr4[3] = {ptr1,ptr2,ptr3};
	int** arr3[] = {arr,arr4};
*/

}
