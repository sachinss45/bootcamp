#include<stdio.h>


void main(){

	int x = 10;
	int y = 20;
	int z = 30;

	
	int* arr[3] = {&x,&y,&z};
	/*Representation
		------   ------  ------
	       x| 10 | y | 20 | z| 30 |
		------   ------  ------
		100      200      300
	     
		--------------------
	    arr	| 100 |  104 | 108 |
		--------------------
		200    208    216   223

	*/
	printf("%ld\n",sizeof(arr));//8*3 = 24

	printf("%p\n",arr[0]);//100
	printf("%p\n",arr[1]);//104
	printf("%p\n",arr[2]);//108

	printf("%d\n",*(arr[0]));//10
	printf("%d\n",*(arr[1]));//20
	printf("%d\n",*(arr[2]));//30
				
}
