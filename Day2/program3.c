#include<stdio.h>
/*
 	a
	A	B
	a	b	c
	A	B	C	D
 */
void main(){

	int n;
	printf("Enter number of rows: ");
	scanf("%d",&n);

	for(int row=1;row<=n;row++){
		int temp = 65;
		for(int col= 1  ; col<=row  ; col++  ){
			if(row&1){
				printf("%c\t",temp+32);
			}else{
				printf("%c\t",temp);
			}
			temp++;
		}
		printf("\n");
	}

}

