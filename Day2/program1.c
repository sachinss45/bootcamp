#include<stdio.h>
/*
   1
   3   5
   7   9   11
   13  15  17  19
 */
void main(){

	int n;
	printf("Enter number of rows: ");
	scanf("%d",&n);
	int temp = 1;
	for(int row=1;row<=n;row++){

		for(int col=1; col<=row; col++){

			printf("%d\t",temp+=2);
		}
		printf("\n");
	}

}

