#include<stdio.h>
/*
 	D	C	B	A
	c	b	a
	B	A
	a
 */
void main(){

	int n;
	printf("Enter number of rows: ");
	scanf("%d",&n);

	for(int row=1;row<=n;row++){
		int temp =65 + n - row;
		for(int col= n  ; col>=row  ; col--  ){
			if(row&1){
				printf("%c\t",temp);
			}else{
				printf("%c\t",temp+32);
			}
			temp = temp -1;

		}
		printf("\n");
	}

}

