#include<stdio.h>
/*
 	D	C 	B 	A
	e 	f	g	h
	F	E	D	C
	g	h	i	j
 */
void main(){

	int n;
	printf("Enter number of rows: ");
	scanf("%d",&n);

	for(int row=1;row<=n;row++){
		int temp = 63+n+row;
		for(int col=1; col<=n; col++){
			
			if(row&1){
				printf("%c\t",temp);
				temp--;
			}else{
				printf("%c\t",temp+32);
				temp++;
			}
		}
		printf("\n");
	}

}

