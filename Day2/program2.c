#include<stdio.h>
/*
 	18   16   14
	12   10   8
	6    4    2
 */
void main(){

	int n;
	printf("Enter number of rows: ");
	scanf("%d",&n);
	int temp = (n*n)*2;
	for(int row=1;row<=n;row++){

		for(int col= 1  ; col<=n; col++  ){
			
			printf("%d\t",temp);
			temp = temp -2;
		}
		printf("\n");
	}

}

